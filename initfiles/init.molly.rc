import init.tegra.rc
import init.molly_common.rc
import init.t114.rc
import init.molly.led.rc

on post-fs

    # enable mlan once /system/vendor/firmware is available
    write /sys/board_properties/mlan/enable 1

on post-fs-data

    # Disable mollyled's "pulsing" value
    write /sys/devices/platform/molly-led/pulsing 0

on boot

    chown system system /sys/kernel/debug/aahbt_input/enable_dpad

    # set a higher min_free_kbytes (16MB) so smsc ethernet driver
    # doesn't drop packets when turbo mode is used
    write /proc/sys/vm/min_free_kbytes 16384

    # Set console mode
    setprop persist.tegra.hdmi.primary 1
    setprop persist.sys.hdmi.resolution Max
    setprop persist.sys.display.resolution 1920x1080

    # Power management settings
    # Audio glitches can occur if we drop below 204MHz.  To reduce
    # risk/complexity further, we just disable LP core.  GP core
    # min freq is 306MHz.
    write /sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/no_lp 1

    # Throughput hinting is designed to save power by slowing down the gpu
    # if it is beating the desired frame.  Without hinting, the gpu frequency
    # scaler operates just by watching the load.
    # When enabled, throughput hinting is only used if there is only one "app"
    # running, which is counted by the number of egl contexts that are
    # bound to window surfaces.  However, the current implementation flipflops
    # the app count when an app is animating content on two window surfaces
    # simulataneously.  This amounts to many alternating calls to eglMakeCurrent
    # on one context and two different surfaces.  When the app count flip flops
    # we get inconsistent provision of hints, and this prevents the gpu frequency
    # from scaling up as it should.
    # Until this is fixed, throughput hinting is universally disabled here.
    # Note that most of the time on Molly the hinting was unused anyway
    # since the live wallpaper keeps its context alive even while
    # invisible so the app count is always >= 2.
    # In order to use the hinting we'll need to have the wallpaper tear down
    # down its context appropriately.
    write /d/scaling/use_throughput_hint 0

    # nvdps
    chown system system /sys/class/graphics/fb0/device/nvdps

    # Wifi firmware reload path 
    chown wifi wifi /sys/module/wlan/parameters/fwpath

on fs

    mkdir /factory 0775 radio radio

    # We chown/chmod /factory because mount is run as root + defaults
    chown radio radio /factory
    chmod 0775 /factory

    # permissions for HDMI-CEC.
    chown system system /sys/devices/platform/tegra_cec/cec_logical_addr_config
    chmod 0660 /sys/devices/platform/tegra_cec/cec_logical_addr_config
