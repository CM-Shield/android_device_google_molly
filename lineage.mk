# Inherit device configuration for molly.
$(call inherit-product, device/google/molly/full_molly.mk)

# Inherit some common lineage stuff.
ifeq ($(ALTERNATE_BUILD),true)
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)
else
$(call inherit-product, vendor/cm/config/common_full_tv.mk)
endif

PRODUCT_NAME := lineage_molly
PRODUCT_DEVICE := molly
